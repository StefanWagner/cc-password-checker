package com.example.worker.Controller_Stuff;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClientBuilder;
import com.amazonaws.services.sqs.model.Message;
import com.amazonaws.services.sqs.model.ReceiveMessageRequest;
import com.example.worker.Networking.Internal_Objects.LookUpTask;
import com.example.worker.Networking.Internal_Objects.ProgressMessage;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

public class WorkerConnectorImp implements WorkerConnector {

    private static Logger logger = LoggerFactory.getLogger(WorkerConnectorImp.class);

    static final String pushTaskUrl = "https://sqs.us-east-1.amazonaws.com/656465555881/PushTasks";
    static final String progressUrl = "https://sqs.us-east-1.amazonaws.com/656465555881/Progress";
    static final String interruptUrl = "https://sqs.us-east-1.amazonaws.com/656465555881/Interrupt";
    private static AmazonSQS sqs = AmazonSQSClientBuilder.standard().withRegion(Regions.US_EAST_1).build();
    private static final ObjectMapper mapper = new ObjectMapper();
    private int total_no_rc = 0;
    List<Message> messageList = new ArrayList<>();

    private static Map<String, List<LookUpTask>> taskMap = new HashMap<>();
    ReceiveMessageRequest receiveMessageRequest = new ReceiveMessageRequest(progressUrl);

    @Override
    public String pushTasks(List<LookUpTask> lookUpTasks) {
        String uid = UUID.randomUUID().toString();
        this.taskMap.put(uid, lookUpTasks);

        for (LookUpTask task : lookUpTasks) {
            sqs.sendMessage(pushTaskUrl, task.toString());
        }
        return uid;
    }

    @Override
    public int handleProgress(String tasksID) {


        if (!this.taskMap.containsKey(tasksID)) {
           // logger.error(tasksID + " does not exist (anymore?).");
            return -1;
        }

        receiveMessageRequest.setMaxNumberOfMessages(10);
        messageList = sqs.receiveMessage(receiveMessageRequest).getMessages();

        if (messageList.isEmpty()) {
            return -1;
        }


        for (Message message : messageList) {
            ProgressMessage msg = null;
            try {
                msg = mapper.readValue(message.getBody(), ProgressMessage.class);
            } catch (Exception e) {
               // logger.error(e.getMessage());
                return -1;
            }

            for (LookUpTask task : this.taskMap.get(tasksID)) {
                if (task.getId().equals(msg.getTaskID())) {
                    sqs.deleteMessage(progressUrl, message.getReceiptHandle());

                    if (msg.getIndex() == -1) {
                        logger.info("total no_rcv: "+ ++total_no_rc);
                        logger.info("Got -1 from worker.");

                        System.out.println("size before: " + taskMap.get(tasksID).size());

                        taskMap.get(tasksID).remove(task);

                       System.out.println("size after: " + taskMap.get(tasksID).size());

                        if (this.taskMap.get(tasksID).size() == 0) {
                            taskMap.remove(tasksID);
                            return -2;
                        } else {
                            return -1;
                        }
                    } else {
                        interruptTasks(tasksID);
                        logger.info("Returning index: " + msg.getIndex());
                        taskMap.remove(tasksID);
                        return msg.getIndex();
                    }
                }
            }
        }
        //System.out.println("SHOULD NOT HAPPEN!");
        return -1;
    }
     /*
        Message message = messageList.get(0);
        try {
            msg = mapper.readValue(message.getBody(), ProgressMessage.class);
        } catch (Exception e) {
            logger.error(e.getMessage());
            //e.printStackTrace();
            return -1;
        }
        for (LookUpTask task : this.taskMap.get(tasksID)) {
            if (task.getId().equals(msg.getTaskID())) {
                sqs.deleteMessage(progressUrl, message.getReceiptHandle());
                if(msg.getIndex() == -1) {
                    logger.info("Got -1 from worker." );
                    if(this.taskMap.get(tasksID).size() == 0) {
                        taskMap.remove(tasksID);
                        return -2;
                    }
                    else {
                        taskMap.get(tasksID).remove(task);
                        if(taskMap.get(tasksID).isEmpty()) {
                            taskMap.remove(tasksID);
                            return -2;
                        }
                        return -1;
                    }
                } else {
                    interruptTasks(tasksID);
                    logger.info("Returning index: " + msg.getIndex());
                    taskMap.remove(tasksID);
                    return msg.getIndex();
                }
            }
        }
        return -1;
    }
*/
    private void interruptTasks(String tasksID) {
        for (LookUpTask task : taskMap.get(tasksID)) {
            logger.info("Trying to interrupt: " + tasksID);
            sqs.sendMessage(interruptUrl, task.getId());
        }
    }

    public List<LookUpTask> getPendingTasks(String tasksID) {
        if(!this.taskMap.containsKey(tasksID)) {
            logger.error(tasksID + " does not exist (anymore?).");
            return new ArrayList<>();
        }
        else {
            return taskMap.get(tasksID);
        }
    }
}

