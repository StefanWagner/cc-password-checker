package com.example.worker;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClientBuilder;
import com.amazonaws.services.sqs.model.Message;
import com.example.worker.Controller_Stuff.WorkerConnector;
import com.example.worker.Controller_Stuff.WorkerConnectorImp;
import com.example.worker.Networking.Internal_Objects.LookUpTask;
import com.example.worker.Networking.MessageParser;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.sql.SQLOutput;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
class WorkerApplicationTests {
    /**
     * Checks the mapping from Message to LookUpTask
     */
    @Test
    void lookupMapping() {
        ArrayList<String> keys = new ArrayList<>();
        keys.add("bla");
        keys.add("bla");
        keys.add("bla");
        ArrayList<Integer> starts = new ArrayList<Integer>();
        ArrayList<Integer> ends = new ArrayList<>();

        starts.add(1);
        starts.add(1);
        starts.add(1);
        starts.add(1);

        ends.add(2);
        ends.add(2);
        ends.add(2);
        ends.add(2);
        ends.add(2);

        LookUpTask task = new LookUpTask(keys, "id_01", "sha12", ends, starts);

        Message msg = new Message();
        msg.setBody(task.toString());


        LookUpTask shouldbe = MessageParser.parseTaskMsg(msg);


        assertTrue(task.toString().equals(shouldbe.toString()));
    }

    /**
     * Tests if the worker finds the String lookFor in list1
     */
    @Test
    void connectorStub() {
        String lookFor = "020308ECFB0766380643F35D72161EA5F0F36587:4";
        WorkerConnector workerConnector = new WorkerConnectorImp();

        //public LookUpTask(ArrayList<String> awsKeys, String id, String lookFor, ArrayList startIndeces, ArrayList endIndeces)

        ArrayList<String> aws = new ArrayList<>();
        aws.add("list/list1");
        ArrayList<Integer> start = new ArrayList<>();
        start.add(0);
        ArrayList<Integer> end = new ArrayList<>();
        end.add(5000000);
        String id = "TASK1";
        LookUpTask lookUpTask1 = new LookUpTask(aws, id, lookFor, start, end);

        ArrayList<String> aws2 = new ArrayList<>();
        aws2.add("list/list1");
        ArrayList<Integer> start2 = new ArrayList<>();
        start2.add(5000000);
        ArrayList<Integer> end2 = new ArrayList<>();
        end2.add(10000000);
        String id2 = "TASK2";
        LookUpTask lookUpTask2 = new LookUpTask(aws2, id2, lookFor, start2, end2);

        ArrayList<LookUpTask> tasks = new ArrayList<>();
        tasks.add(lookUpTask1);
        tasks.add(lookUpTask2);

        String tasksID = workerConnector.pushTasks(tasks);

        int index = -1;
        while((index = workerConnector.handleProgress(tasksID)) == -1);
        System.out.println(index);

        assertTrue(index == 4504317);
    }


    @Test
    void lookForString_whole_List(){

        String lookfor ="5BAA61E4C9B93F3F0682250B6CF8331B7EE68FD8x";


        WorkerConnector workerConnector = new WorkerConnectorImp();

        Map<String,Integer> map = new HashMap<>();

        for(int i=1;i<=58;i++){
            ArrayList listname =new ArrayList<String>();
            listname.add("list/list"+i);
            ArrayList startslist = new ArrayList();
            startslist.add(0);
            ArrayList endlist =  new ArrayList();

            if(i==58)
                endlist.add(2_611_620);
            else
                endlist.add(10_000_000);

            ArrayList<LookUpTask> tasklist = new ArrayList<>();
            tasklist.add(
                    new LookUpTask(listname,"uniquetaskid_"+i,lookfor,startslist,endlist));


            String uid = workerConnector.pushTasks(tasklist);
            map.put(uid,i);
          //  System.out.println(uid);
            System.out.println(i);
        }

       System.out.println("Lookfor: "+lookfor);
        System.out.println(map.size());

        int total = 0;
        for(;total<58;){
            for(String s:map.keySet()){
                int ret = workerConnector.handleProgress(s);
                switch (ret){
                    case -1:
                        break;
                    case -2:
                        System.out.println("Definitly not found File: "+map.get(s));
                        total++;
                        break;
                    default:
                        System.out.println("Found in File: "+map.get(s));
                        total = 60;
                        break;
                }
            }
            System.out.println(""+total+" of "+map.keySet().size()+" returned -1");
        }

        System.out.println("check how many messages in queue!");
    }


    /**
     * Tests if the worker finds a password in the 10th file, when providing 10 files
     */
    @Test
    void connectorStub2_multiple_Files_for_worker() {
        String File_10 = "2870559C47C8D4C00ED8C1182949EA40B0C686D4:2";

        ArrayList<String> aws = new ArrayList<>();
        ArrayList<Integer> start = new ArrayList<>();
        ArrayList<Integer> end = new ArrayList<>();


        for (int i = 1; i <= 10; i++) {

            aws.add("list/list" + i);
            start.add(0);
            end.add(10000000);

        }

        LookUpTask task = new LookUpTask(aws, "id_1", File_10, start, end);

        ArrayList<LookUpTask> list = new ArrayList<>();
        list.add(task);

        WorkerConnector workerConnector = new WorkerConnectorImp();

        String tasksID = workerConnector.pushTasks(list);

        while (workerConnector.handleProgress(tasksID) == -1) {
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        assertTrue(true);
    }

    /**
     * Checks if handleProgress method removes every LookUpTask from the list and checks if getPendingTasks therefore will send an empty list
     */
    @Test
    void testIfPendingTasksDecrementsCorrectly() {
        String nope = "YOU_WONT_FIND_ME";
        ArrayList<String> aws = new ArrayList<>();
        aws.add("list/list1");
        ArrayList<Integer> start = new ArrayList<>();
        ArrayList<Integer> end = new ArrayList<>();
        start.add(0);
        end.add(1000);

        WorkerConnector connector = new WorkerConnectorImp();

        List<LookUpTask> tasks = new ArrayList<>();
        for(int i = 0; i < 5; ++i) {
            LookUpTask task = new LookUpTask(aws, "id"+i, nope, start, end);
            tasks.add(task);
        }
        String tasksID = connector.pushTasks(tasks);

        while(connector.handleProgress(tasksID) == -1) {
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        assertTrue(connector.getPendingTasks(tasksID).size() == 0);
    }

    /**
     * Checks if handleProgress method response correctly for not available taskID.
     */
    @Test
    void testIfHandleProgressReturnsRightIfNotAvailableID() {
        WorkerConnector connector = new WorkerConnectorImp();

        int response = connector.handleProgress("NOT_IN");
        assertTrue(response == -1);
    }


    /**
     * Tests if communication via PushTaskQueue works.
     */
    @Test
    void testIfPushTaskQueueSendingAndReceivingWorks() {
        String pushTaskUrl ="https://sqs.us-east-1.amazonaws.com/656465555881/PushTasks";
        AmazonSQS sqs = AmazonSQSClientBuilder.standard().withRegion(Regions.US_EAST_1).build();
        sqs.sendMessage(pushTaskUrl, "Test");
        List<Message> messages = sqs.receiveMessage(pushTaskUrl).getMessages();
        String received = messages.get(0).getBody();
        assertTrue(received.equals("Test"));

    }

    /**
     * Tests if communication via ProgressQueue works.
     */
    @Test
    void testIfProgressQueueSendingAndReceivingWorks() {
        String progressUrl ="https://sqs.us-east-1.amazonaws.com/656465555881/Progress";
        AmazonSQS sqs = AmazonSQSClientBuilder.standard().withRegion(Regions.US_EAST_1).build();
        sqs.sendMessage(progressUrl, "Test");
        List<Message> messages = sqs.receiveMessage(progressUrl).getMessages();
        String received = messages.get(0).getBody();
        assertTrue(received.equals("Test"));
    }

    /**
     * Tests if communication via InterruptQueue works.
     */
    @Test
    void testIfInterruptQueueSendingAndReceivingWorks() {
        String interruptUrl ="https://sqs.us-east-1.amazonaws.com/656465555881/Interrupt";
        AmazonSQS sqs = AmazonSQSClientBuilder.standard().withRegion(Regions.US_EAST_1).build();
        sqs.sendMessage(interruptUrl, "Test");
        List<Message> messages = sqs.receiveMessage(interruptUrl).getMessages();
        String received = messages.get(0).getBody();
        assertTrue(received.equals("Test"));
    }
}
