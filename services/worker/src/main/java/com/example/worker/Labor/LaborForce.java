package com.example.worker.Labor;

import com.example.worker.Networking.Communicator;
import com.example.worker.Networking.Internal_Objects.LookUpTask;
import com.example.worker.Networking.Internal_Objects.ThreadLevelTask;
import com.example.worker.Storage.FileHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.*;
import java.io.*;
import java.util.ArrayList;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

public class LaborForce {

    private static Logger logger = LoggerFactory.getLogger(LaborForce.class);
    private static final int numberThreads = 4;
    private ThreadPoolExecutor executor = (ThreadPoolExecutor) Executors.newFixedThreadPool(numberThreads);
    private ArrayList<ThreadLevelTask> tasks = new ArrayList<>();
    private volatile boolean done = false;
    private volatile int where = 0;
    private volatile int threadsFinished = 0;
    private volatile boolean interrupted = false;


    public void distributeTask(LookUpTask task) {
        interrupted = false;

        logger.info("Looking for: "+task.getLookFor());
        String fileNames = "";
        for(String fileName : task.getAwsKeys()) {
            fileNames += fileName+", ";
        }
        logger.info("Looking in: " + fileNames);

        if(Communicator.amIinterrupted()) {
            logger.info("I am already interrupted.");
            return;
        }

        for (int i = 0; i < task.getStartIndeces().size(); i++) {
            Communicator.retrieveAndGenerateFile(task.getAwsKeys().get(i));
            setRanges(task, i);

            this.done = false;
            this.where = -1;
            this.threadsFinished = 0;

            for (int j = 0; j < numberThreads; j++) {
                logger.info("task" + j + " gets his work!");

                final ThreadLevelTask threadtask = tasks.get(j);
                executor.submit(() -> doTask(threadtask));
            }

            while(!done && !(threadsFinished>=numberThreads));
            if (where != -1) {
                logger.info("Found at: " + where);
                Communicator.tellCollector(where);
                return;
            }
            else if(interrupted){
                return;
            } else{
                logger.info("I am done with this file, but did not find it.");
            }
        }
        Communicator.tellCollector(-1);
        logger.info("I am done with all my files, but did not find it.");
    }

    private void doTask(ThreadLevelTask task) {
        logger.info("Starting task");

        int to = task.getTo();
        File file = task.getFile();
        boolean found = false;
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new FileReader(file));
        } catch (FileNotFoundException e) {
            logger.error(e.getMessage());
            System.exit(0);
        }

        logger.info("Initializing of stuff ready in Task");

        for (int i = 0; i < task.getFrom(); i++) {
            try {
                reader.readLine();
            } catch (IOException e) {
                logger.error(e.getMessage());
                System.exit(0);
            }
        }

        logger.info("Skipped my part of the file");

        String new_line = null;
        String line = "_";
        for (int i = task.getFrom(); i < to; i++) {
            if (i % 100000 == 0) {
                if (Communicator.amIinterrupted()) {
                    logger.info("I AM INTERRUPTED");
                    done = true;
                    interrupted = true;
                }
            }

            if (done) {
                break;
            }

            try {
                new_line = reader.readLine();
                found = new_line
                        .substring(0, task.getLookfor().length())
                        .equals(task.getLookfor());
            } catch (IOException e) {
                logger.error(e.getMessage());
                System.exit(0);
            }


            if (found) {
                if (!line.equals(new_line.substring(0,task.getLookfor().length())) && !line.equals("_")) {
                    where = i;
                    logger.info("I FOUND IT!!! LookFor: " + task.getLookfor() + "line: " + line + "new_line: "+new_line + "new_line.substring(0,"+task.getLookfor().length()+")="+new_line.substring(0, task.getLookfor().length()) );
                    done = true;
                }
                break;
            }

            line = new_line.substring(0, task.getLookfor().length());
        }
        logger.info("I finished.... Threads finished: "+ ++threadsFinished);
    }


    private void setRanges(LookUpTask task, int fileNr) {
        //always 10mil lines per File

        logger.info("Start index: " + task.getStartIndeces());
        logger.info("End index: " + task.getEndIndeces());


        int range = 1+task.getEndIndeces().get(fileNr) - task.getStartIndeces().get(fileNr);
        int subtract = (task.getStartIndeces().get(fileNr)==0) ? 0 : 1;

        for (int i = 0; i < numberThreads; i++) {
            int from = (range * i / numberThreads) + task.getStartIndeces().get(fileNr) - subtract;
            int to = (range * (i + 1) / numberThreads) + task.getStartIndeces().get(fileNr);
            logger.info("FROM: " + from + ", TO: " + to);
            this.tasks.add(i, new ThreadLevelTask(
                    FileHandler.getFile(),
                    from, to, task.getLookFor()));
        }
    }
}