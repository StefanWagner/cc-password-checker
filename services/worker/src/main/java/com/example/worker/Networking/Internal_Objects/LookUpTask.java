package com.example.worker.Networking.Internal_Objects;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class LookUpTask {
    private static Logger logger = LoggerFactory.getLogger(LookUpTask.class);

    private String id; //task id
    private String lookFor; // The actual SHA hashing that we are looking for, e.g.: AAAAAABCC

    private ArrayList<String> awsKeys; //file names, e.g.: /list/list1
    private ArrayList<Integer> endIndices;
    private ArrayList<Integer> startIndices; //startIndex [0] must be the same as last index of task before

    @Override
    public String toString() {
        try {
            return new ObjectMapper().writeValueAsString(this);
        } catch (JsonProcessingException e) {
            logger.error(e.getMessage());
            System.exit(0);
        }
        return null;
    }

    public LookUpTask() {
        awsKeys = new ArrayList<>();
        endIndices = new ArrayList<>();
        startIndices = new ArrayList<>();
        id = "";
        lookFor = "";

    }

    public LookUpTask(ArrayList<String> awsKeys, String id, String lookFor, ArrayList startIndeces, ArrayList endIndeces) {
        this.awsKeys = awsKeys;
        this.id = id;
        this.lookFor = lookFor;
        this.startIndices = startIndeces;
        this.endIndices = endIndeces;
    }

    public boolean equals(Object task){
        return ((LookUpTask)(task)).getId().equals(this.id);
    }


    public ArrayList<String> getAwsKeys() {
        return awsKeys;
    }

    public void setAwsKeys(ArrayList awsKey) {
        this.awsKeys = awsKey;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLookFor() {
        return lookFor;
    }

    public void setLookFor(String lookFor) {
        this.lookFor = lookFor;
    }

    public ArrayList<Integer> getEndIndeces() {
        return endIndices;
    }

    public void setEndIndeces(ArrayList<Integer> endIndeces) {
        this.endIndices = endIndeces;
    }

    public ArrayList<Integer> getStartIndeces() {
        return startIndices;
    }

    public void setStartIndeces(ArrayList<Integer> startIndeces) {
        this.startIndices = startIndeces;
    }


}


