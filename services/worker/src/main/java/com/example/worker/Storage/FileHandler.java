package com.example.worker.Storage;


import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;

public class FileHandler {
    private static Logger logger = LoggerFactory.getLogger(FileHandler.class);

    private static File file = null;

    public static void saveToFile(InputStream s3is) {

        file = new File("file");


        try {
            FileUtils.copyInputStreamToFile(s3is, file);
        } catch (Exception e) {
            logger.error("saving to File failed:" + e);
        }
    }

    public static File getFile() {
        return file;
    }
}
