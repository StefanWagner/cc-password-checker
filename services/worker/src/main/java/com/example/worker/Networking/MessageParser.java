package com.example.worker.Networking;

import com.amazonaws.services.sqs.model.Message;
import com.example.worker.Networking.Internal_Objects.LookUpTask;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import java.util.ArrayList;

//Parses External Messages to Internal Messages
//If something in the External Representation of the Objects changes, we just need to fix it here not in the whole Application.
public class MessageParser {

    private static Logger logger = LoggerFactory.getLogger(MessageParser.class);

    public static LookUpTask parseTaskMsg(Message msg){
        LookUpTask task = null;

        ObjectMapper objectMapper = new ObjectMapper();
        try {
            task = objectMapper.readValue(msg.getBody(), LookUpTask.class);
        } catch (JsonProcessingException e) {
            logger.error(e.getMessage());
            System.exit(0);
        }
        return task;

    }

}
