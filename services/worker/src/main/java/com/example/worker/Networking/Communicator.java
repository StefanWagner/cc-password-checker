package com.example.worker.Networking;

import com.amazonaws.auth.*;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClientBuilder;
import com.amazonaws.services.sqs.model.Message;
import com.amazonaws.services.sqs.model.ReceiveMessageRequest;
import com.example.worker.Networking.Internal_Objects.ProgressMessage;
import com.example.worker.Networking.Internal_Objects.LookUpTask;
import com.example.worker.Storage.FileHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.List;

public class Communicator {

    private static String  taskID = "";
    private static int fileNr = 0;

    private static String bucket_name = "passwordchecker";
    private static String bucket_name2 = "passwordchecker";
    private static final AmazonS3 s3 = AmazonS3ClientBuilder.standard().withRegion(Regions.US_EAST_1).build();

    static InstanceProfileCredentialsProvider provider = InstanceProfileCredentialsProvider.getInstance();

    private static AmazonSQS sqs = AmazonSQSClientBuilder.standard().withRegion(Regions.US_EAST_1)/*.withCredentials(provider)*/.build();

    static final String pushTaskUrl ="https://sqs.us-east-1.amazonaws.com/656465555881/PushTasks";
    static final String progressUrl ="https://sqs.us-east-1.amazonaws.com/656465555881/Progress";
    static final String interruptUrl ="https://sqs.us-east-1.amazonaws.com/656465555881/Interrupt";


    private static Logger logger = LoggerFactory.getLogger(Communicator.class);


    public static boolean amIinterrupted(){


        ReceiveMessageRequest receiveMessageRequest = new ReceiveMessageRequest(interruptUrl);
        receiveMessageRequest.setMaxNumberOfMessages(10);
        List<Message> listMessage = sqs.receiveMessage(receiveMessageRequest).getMessages();

        if(listMessage.isEmpty()) {
            //logger.info("Received empty interrupted list.");
            return false;
        }

        logger.info("interrupt messages total: "+listMessage.size());

       for(Message msg : listMessage) {
           if (msg.getBody().equals(taskID)) {
               logger.info("I am going to interrupt.");
               sqs.deleteMessage(interruptUrl, msg.getReceiptHandle());
               return true;
           }
       }
        logger.info("I am NOT going to interrupt.");
        return false;
    }


    public static void tellCollector(int index){
        if(index == -1) {
            logger.info("Did not find the string. Sending -1 to progress queue.");
            ProgressMessage progressMessage = new ProgressMessage(taskID, index);
            sqs.sendMessage(progressUrl,progressMessage.toString());
        } else {
            index = 10000000*(fileNr-1)+index;
            logger.info("Telling collector that we found the requested string in this file at index "+index);
            ProgressMessage progressMessage = new ProgressMessage(taskID, index);
            sqs.sendMessage(progressUrl,progressMessage.toString());
        }
    }

    //checks the MessageQueue for Tasks
    public static LookUpTask checkForTasks(){
        List<Message> listMessage = sqs.receiveMessage(pushTaskUrl).getMessages();
        if(listMessage.isEmpty()) {
            return null;
        }
        Message msg = listMessage.get(0);

        if(msg!=null) {

            sqs.deleteMessage(pushTaskUrl, msg.getReceiptHandle());
            LookUpTask task =  MessageParser.parseTaskMsg(msg); // parse it to internal Object representationreturn
            taskID = task.getId();
            return task;
        }

        return null;

    }


    public static void retrieveAndGenerateFile(String key){
           retrieveMs3File(key);
    }


    private static void retrieveMs3File(String key_name){
        try {
            fileNr =Integer.parseInt(key_name.substring(9,key_name.length()));
            InputStream s3is = s3.getObject(bucket_name, key_name).getObjectContent();
            FileHandler.saveToFile(s3is);
            return;
        } catch (Exception e) {
            logger.error(e.getMessage());
            logger.info("Retrieving file from Patrick's bucket did not work. Trying on Pauls bucket.");
        }
        try {
            fileNr =Integer.parseInt(key_name.substring(9,key_name.length()));
            InputStream s3is = s3.getObject(bucket_name2, key_name).getObjectContent();
            FileHandler.saveToFile(s3is);
            return;
        } catch (Exception e) {
            logger.error(e.getMessage());
            System.exit(0);
        }
    }
}
