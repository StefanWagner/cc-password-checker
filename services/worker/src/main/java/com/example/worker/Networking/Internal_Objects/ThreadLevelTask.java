package com.example.worker.Networking.Internal_Objects;

import java.io.File;
import java.lang.reflect.Array;
import java.util.ArrayList;

public class ThreadLevelTask {
    public ThreadLevelTask(File file, int from, int to, String lookfor) {
        this.file = file;
        this.from = from;
        this.to = to;
        this.lookfor = lookfor;
    }

    private String lookfor = "";
    private File file = null;
    private int from = 0;
    private int to = 0;

    public int getFrom() {
        return from;
    }

    public void setFrom(int from) {
        this.from = from;
    }

    public int getTo() {
        return to;
    }

    public void setTo(int to) {
        this.to = to;
    }

    public File getFile() { return file; }

    public void setFile(File file) { this.file = file; }

    public String getLookfor() {
        return lookfor;
    }

    public void setLookfor(String lookfor) {
        this.lookfor = lookfor;
    }



}
