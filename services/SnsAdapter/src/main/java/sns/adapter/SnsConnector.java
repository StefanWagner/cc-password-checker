package sns.adapter;

import software.amazon.awssdk.auth.credentials.*;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.sns.SnsAsyncClient;
import software.amazon.awssdk.services.sns.model.*;

import java.util.concurrent.ExecutionException;

public class SnsConnector {
    private final SnsAsyncClient snsClient;

    /**
     * Liest credentials aus der laufenden EC2 umgebung. Für deployment auf aws
     */
    public SnsConnector() {
        snsClient = SnsAsyncClient.builder()
                .region(Region.US_EAST_1)
                .credentialsProvider(InstanceProfileCredentialsProvider.create())
                .build();
    }

    /**
     * Benutzt die übergebenen daten, bsp. übergeben von der sns.adapter.Configuration klasse
     *
     * @param awsAccessKeyId aws id von der sns.adapter.Configuration klasse
     * @param awsAccessSecretKey aws secret key aus sns.adapter.Configuration
     */
    public SnsConnector(String awsAccessKeyId, String awsAccessSecretKey, String awsSessionToken){
        AwsSessionCredentials awsCreds = AwsSessionCredentials.create(awsAccessKeyId
                , awsAccessSecretKey
                , awsSessionToken);
        snsClient = SnsAsyncClient.builder()
                .region(Region.US_EAST_1)
                .credentialsProvider(StaticCredentialsProvider.create(awsCreds))
                .build();
    }

    public boolean subscribeTopic(String protocol, Topics topic, String targetURL){
        try {
            SubscribeRequest request = SubscribeRequest.builder()
                    .protocol(protocol)
                    .endpoint(targetURL)
                    .returnSubscriptionArn(true)
                    .topicArn(topic.topicARN)
                    .build();

            SubscribeResponse result = snsClient.subscribe(request).get();
            System.out.println("Subscription ARN: " + result.subscriptionArn() + "\n\n Status was " + result.sdkHttpResponse().statusCode());

            return true;
        } catch (SnsException e) {
            System.err.println(e.awsErrorDetails().errorMessage());
            System.exit(1);
        } catch (InterruptedException | ExecutionException e) {
            System.err.println(e.getMessage());
            e.printStackTrace();
            System.exit(1);
        }
        return false;
    }

    public boolean sendMessage(String message, Topics topic) {
        try {
            PublishRequest request = PublishRequest.builder()
                    .message(message)
                    .topicArn(topic.topicARN)
                    .build();

            PublishResponse result = snsClient.publish(request).get();
            return result.sdkHttpResponse().isSuccessful();
        } catch (SnsException e) {
            System.err.println(e.awsErrorDetails().errorMessage());

        } catch (InterruptedException | ExecutionException e) {
            System.err.println(e.getMessage());
            e.printStackTrace();
        }
        return false;
    }
}
