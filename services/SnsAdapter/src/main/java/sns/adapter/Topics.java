package sns.adapter;

public enum Topics {
    submission("arn:aws:sns:us-east-1:600282955681:submission_public"),
    progress("arn:aws:sns:us-east-1:434699489654:progress_public");

    public final String topicARN;

    Topics(String topicARN) {
        this.topicARN = topicARN;
    }
}
