package controller.demo;

public enum LookUpTaskResult {
    Pending,
    NotFound,
    Found;

    private int result;

    private LookUpTaskResult() {
    }

    private LookUpTaskResult setResult(int result){
        this.result = result;
        return this;
    }

    public static LookUpTaskResult getTaskResultFromCode(int code) {
        if (code == -1) {
            return  Pending.setResult(code);
        } else if (code == -2) {
            return  NotFound.setResult(code);
        } else if (code >= 0) {
            return Found.setResult(code);
        }
        throw new RuntimeException("Invalid LookupTask result code");
    }

    public int getResult() {
        return result;
    }
}
