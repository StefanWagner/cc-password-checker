package controller.demo.frontendConnection;


/**
 * FrontendConnector calls controller class method with this Object
 * when a new user submission arrived.
 */
public class UserSubmission {
    private String uuid;
    private String password;

    public UserSubmission(String uuid, String password) {
        this.uuid = uuid;
        this.password = password;
    }

    public String getUuid() {
        return uuid;
    }

    public String getPassword() {
        return password;
    }
}
