package controller.demo.frontendConnection;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import controller.demo.SearchController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import sns.adapter.SnsConnector;
import sns.adapter.Topics;

@RestController
public class FrontendConnectionController {
    private static final Logger log = LoggerFactory.getLogger(FrontendConnectionController.class);
    private static final SnsConnector snsAdapter = new SnsConnector();
    private static final UserSubmissionController userSubmissionController = new SearchController();


    @PostMapping("/message")
    public ResponseEntity<?> receiveMessage(@RequestBody String body) {
        log.info("\n--------SNS Message Received----------\n" + body);

        String extractedMessage = extractJsonStringFromSnsNotification(body);
        if (!extractedMessage.isEmpty()) {
            processJsonMessage(extractedMessage);
        }

        return ResponseEntity.ok().build();
    }


    public synchronized static boolean pushToFrontend(SubmissionProgress message) {
        String jsonMessage = String.format("{'uuid':'%s', 'progress': %d}", message.getUuid(), message.getProgress());
        log.debug("sent progress to sns: "+jsonMessage);
        return snsAdapter.sendMessage(jsonMessage, Topics.progress);
    }

    public synchronized static boolean pushToFrontend(SubmissionResult message) {
        String jsonMessage = String.format("{'uuid':'%s', 'progress': 100, 'not_leaked': %b}", message.getUuid(), message.isNotLeaked());
        log.debug("sent result to sns: "+jsonMessage);
        return snsAdapter.sendMessage(jsonMessage, Topics.progress);
    }


    private static String extractJsonStringFromSnsNotification(String snsNotification) {
        try {
            JsonObject notificationMessage = JsonParser.parseString(snsNotification).getAsJsonObject();
            if (!notificationMessage.get("Type").getAsString().equals("Notification")) {
                return "";
            }
            if (!notificationMessage.has("Message")) {
                throw new RuntimeException("no message provided");
            }
            return notificationMessage.get("Message").getAsString();
        } catch (JsonSyntaxException e) {
            log.error("No valid json provided in sns notification", e);
            return "";
        }
    }

    private static void processJsonMessage(String message) {
        try {
            JsonObject parsedMessage = JsonParser.parseString(message).getAsJsonObject();
            if (parsedMessage.has("uuid")
                    && parsedMessage.has("password")) {
                UserSubmission submission = new UserSubmission(parsedMessage.get("uuid").getAsString()
                        , parsedMessage.get("password").getAsString());
                userSubmissionController.handleUserSubmission(submission);

                log.info("submission received");
            }
        } catch (JsonSyntaxException e) {
            log.error("no valid json in sns message", e);
        }
    }
}
