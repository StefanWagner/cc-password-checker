package controller.demo.frontendConnection;

public class SubmissionProgress {
    private String uuid;
    private int progress;

    public SubmissionProgress(String uuid, int progress) {
        this.uuid = uuid;
        this.progress = progress;
    }

    public String getUuid() {
        return uuid;
    }

    public int getProgress() {
        return progress;
    }
}
