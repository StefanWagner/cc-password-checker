package controller.demo.frontendConnection;

public interface UserSubmissionController {
    /**
     * Must be implemented by the class, which should receive the submissions
     * @param submission password
     */
    public void handleUserSubmission(UserSubmission submission);
}