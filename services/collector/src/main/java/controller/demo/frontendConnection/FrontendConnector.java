package controller.demo.frontendConnection;

public interface FrontendConnector {
    /**
     * Can be called to push progress to the frontend
     * @param progress
     */
    void pushToFrontend(SubmissionProgress message);

    void pushToFrontend(SubmissionResult message);
}
