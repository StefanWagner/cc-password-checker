package controller.demo.frontendConnection;

public class SubmissionResult {
    private String uuid;
    private boolean notLeaked;

    public SubmissionResult(String uuid, boolean notLeaked) {
        this.uuid = uuid;
        this.notLeaked = notLeaked;
    }

    public String getUuid() {
        return uuid;
    }

    public boolean isNotLeaked() {
        return notLeaked;
    }
}
