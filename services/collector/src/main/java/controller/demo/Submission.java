package controller.demo;

import controller.demo.HashSection.GlobalHashSection;
import controller.demo.HashSection.LookUpTaskSection;
import controller.demo.HashSection.SubtaskSection;
import controller.demo.frontendConnection.UserSubmission;
import controller.demo.workerConnection.LookUpTask;

import java.util.ArrayList;
import java.util.UUID;

public class Submission {
    private UserSubmission userSubmission;
    private int currentSearchStringIndex;
    private GlobalHashSection globalHashSection;
    private int amountOfLookUpTasksToCreate;


    public Submission(UserSubmission userSubmission) {
        this.userSubmission = userSubmission;
        globalHashSection = new GlobalHashSection();
        this.currentSearchStringIndex = 1;
        this.amountOfLookUpTasksToCreate = 8;
    }

    /**
     * Creates the next list of tasks (and subtasks) using the
     * globalHashSection
     * @return a list of LookUpTasks to be pushed to the worker
     */
    public ArrayList<LookUpTask> createNextLookUpTasks() {
        ArrayList<LookUpTask> lookUpTasks = new ArrayList<>();
        this.computeCount();
        ArrayList<LookUpTaskSection> lookUpTaskSections = this.globalHashSection.splitIntoLookUpTaskSections(this.amountOfLookUpTasksToCreate);

        for (LookUpTaskSection lookUpTaskSection : lookUpTaskSections) {
            ArrayList<SubtaskSection> subtaskSections = lookUpTaskSection.splitIntoSubtasks();
            String id = UUID.randomUUID().toString();
            String lookFor = this.getSubstring();
            ArrayList<String> awsKeys = new ArrayList<>();
            ArrayList<Integer> startIndices = new ArrayList<>();
            ArrayList<Integer> endIndices = new ArrayList<>();
            for (SubtaskSection subtaskSection : subtaskSections) {
                awsKeys.add(subtaskSection.getFilename());
                startIndices.add(subtaskSection.getLocalIndexStart());
                endIndices.add(subtaskSection.getLocalIndexEnd());
            }
            LookUpTask lookUpTask = new LookUpTask(awsKeys, id, lookFor, startIndices, endIndices);
            lookUpTasks.add(lookUpTask);
        }
        return lookUpTasks;
    }

    public int getCurrentSearchStringIndex() {
        return currentSearchStringIndex;
    }

    public void setCurrentSearchStringIndex(int currentSearchStringIndex) {
        this.currentSearchStringIndex = currentSearchStringIndex;
    }

    public UserSubmission getUserSubmission() {
        return userSubmission;
    }

    public GlobalHashSection getGlobalHashSection() {
        return globalHashSection;
    }

    private void computeCount() {
        int remainingIndexScope = this.globalHashSection.getGlobalEndIndex() - this.globalHashSection.getGlobalStartIndex();
        if (remainingIndexScope < this.amountOfLookUpTasksToCreate*2) {
            this.amountOfLookUpTasksToCreate = 1;
        }
    }

    private String getSubstring() {
        return this.userSubmission.getPassword().substring(0, this.currentSearchStringIndex + 1);
    }

}
