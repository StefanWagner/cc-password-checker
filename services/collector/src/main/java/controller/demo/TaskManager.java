package controller.demo;

import controller.demo.frontendConnection.FrontendConnectionController;
import controller.demo.frontendConnection.SubmissionProgress;
import controller.demo.frontendConnection.SubmissionResult;
import controller.demo.workerConnection.LookUpTask;
import controller.demo.workerConnection.WorkerConnector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;

public class TaskManager extends Thread {
    private static final Logger log = LoggerFactory.getLogger(TaskManager.class);
    private Submission submission;
    private String lookUpTasksId;
    private final WorkerConnector workerConnectorImp;

    public TaskManager(Submission submission, WorkerConnector workerConnector) {
        this.submission = submission;
        this.workerConnectorImp = workerConnector;
    }

    @Override
    public void run() {
        log.info("New Taskmanager started");
        while (this.submission.getCurrentSearchStringIndex() < (this.submission.getUserSubmission().getPassword().length())) {
            log.debug("searching for index {} in password {}"
                    , submission.getCurrentSearchStringIndex()
                    , submission.getUserSubmission().getPassword());
            ArrayList<LookUpTask> lookUpTasks = this.submission.createNextLookUpTasks();
            logLookUpTasks(lookUpTasks);
            this.lookUpTasksId = this.workerConnectorImp.pushTasks(lookUpTasks);
            LookUpTaskResult lookUpTaskResult = this.waitForResult();
            log.debug("worker result is {}", lookUpTaskResult);
            if (lookUpTaskResult.equals(LookUpTaskResult.NotFound)) {
                SubmissionResult submissionResult = new SubmissionResult(this.submission.getUserSubmission().getUuid(), true);
                boolean success = FrontendConnectionController.pushToFrontend(submissionResult);
                log.info("sending to sns was "+ success);
                log.info("password was not found, and frontend notified, shut down thread");
                return;
            } else {
                log.info("substring was found");
                //set smaller section and update index
                SubmissionProgress submissionProgress = new SubmissionProgress(this.submission.getUserSubmission().getUuid(), this.calcProgress());
                boolean success = FrontendConnectionController.pushToFrontend(submissionProgress);
                this.submission.setCurrentSearchStringIndex(this.submission.getCurrentSearchStringIndex() + 2); // Cannot be done inside of submission class because it is a break condition.
                this.submission.getGlobalHashSection().setGlobalStartIndex(lookUpTaskResult.getResult());
                log.info("sending to sns was "+ success);
            }
        }
        SubmissionResult submissionResult = new SubmissionResult(this.submission.getUserSubmission().getUuid(), false);
        boolean success = FrontendConnectionController.pushToFrontend(submissionResult);
        log.info("sending to sns was "+ success);
        log.info("password was found, and frontend notified, shut down thread");
    }

    private LookUpTaskResult waitForResult() {
        LookUpTaskResult lookUpTaskResult = LookUpTaskResult.Pending;
        do {
            log.debug("waiting for worker result: {}", lookUpTaskResult);
            lookUpTaskResult = LookUpTaskResult.getTaskResultFromCode(workerConnectorImp.handleProgress(this.lookUpTasksId));
        } while (lookUpTaskResult.equals(LookUpTaskResult.Pending));
        return lookUpTaskResult;
    }

    private int calcProgress() {
        float done = (float)submission.getCurrentSearchStringIndex();
        float total = (float)(submission.getUserSubmission().getPassword().length() - 1);
        return (int)((done/total) * 100);
    }

    private void logLookUpTasks(ArrayList<LookUpTask> taskList) {
        log.debug("LookUpTasks created:");
        for(int i = 0; i < taskList.size(); i++){
            log.debug("task {}: {}", i+1, taskList.get(i).toString());
        }
    }

}
