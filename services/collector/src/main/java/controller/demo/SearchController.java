package controller.demo;

import controller.demo.frontendConnection.UserSubmission;
import controller.demo.frontendConnection.UserSubmissionController;
import controller.demo.workerConnection.WorkerConnector;
import controller.demo.workerConnection.WorkerConnectorSdk2;
import org.springframework.stereotype.Component;

public class SearchController implements UserSubmissionController {
    private static WorkerConnector workerConnectorImp;

    public SearchController() {
        this.workerConnectorImp = new WorkerConnectorSdk2();
    }

    @Override
    public void handleUserSubmission(UserSubmission userSubmission) {
        Submission submission = new Submission(userSubmission);
        TaskManager taskManager = new TaskManager(submission, workerConnectorImp);
        taskManager.start();
    }

}
