package controller.demo.HashSection;

public class SubtaskSection {
    private String filename;
    private int localIndexStart;
    private int localIndexEnd;

    public SubtaskSection(String filename, int localIndexStart, int localIndexEnd) {
        this.filename = filename;
        this.localIndexStart = localIndexStart;
        this.localIndexEnd = localIndexEnd;
    }

    public String getFilename() {
        return filename;
    }

    public int getLocalIndexStart() {
        return localIndexStart;
    }

    public int getLocalIndexEnd() {
        return localIndexEnd;
    }
}
