package controller.demo.HashSection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;


public class GlobalHashSection {
    private static final Logger log = LoggerFactory.getLogger(GlobalHashSection.class);
    private int globalStartIndex;
    private static final int GLOBAL_END_INDEX = 572_611_620;

    public GlobalHashSection() {
        globalStartIndex = 0;
    }

    /**
     * Split the entire hash list (global hash section) into (count) parts, where
     * the last index of one part is also the first index of the following part.
     *
     * This needs to be done in case a letter transition is separated by two parts. see documentation
     *
     * @param count amount of parts to create
     * @return
     */
    public ArrayList<LookUpTaskSection> splitIntoLookUpTaskSections(int count) {
        ArrayList<LookUpTaskSection> lookUpTasks = new ArrayList<>();
        int sectionSize = calcLookUpTaskSectionSize(count);
        int sectionStartIndex = 0;
        int sectionEndIndex;

        for (int i = 0; i < count-1; i++) {
            sectionStartIndex = globalStartIndex+sectionSize*i;
            //increase by 1 so it contains the first index of the following part
            sectionEndIndex = sectionStartIndex + sectionSize;
            lookUpTasks.add(new LookUpTaskSection(sectionStartIndex, sectionEndIndex));
        }

        //add last section with all remaining indices
        if(count == 1){
            lookUpTasks.add(new LookUpTaskSection(sectionStartIndex, GLOBAL_END_INDEX));
        } else {
            lookUpTasks.add(new LookUpTaskSection(sectionStartIndex+sectionSize, GLOBAL_END_INDEX));
        }
        logLookupTaskSections(lookUpTasks);
        return lookUpTasks;
    }

    public void setGlobalStartIndex(int globalStartIndex) {
        this.globalStartIndex = globalStartIndex;
    }

    public int getGlobalStartIndex() {
        return globalStartIndex;
    }

    public int getGlobalEndIndex() {
        return GLOBAL_END_INDEX;
    }

    private int calcLookUpTaskSectionSize(int count){
        int sectionSize = (GLOBAL_END_INDEX - globalStartIndex)/count;
        if (sectionSize < 1) {
            throw new RuntimeException("global hash section not large enough to split it into "
                    +count+" LookupTaskSections.");
        }
        return sectionSize;
    }

    private void logLookupTaskSections(ArrayList<LookUpTaskSection> sections) {
        log.debug("global indices: [{}, {}]", globalStartIndex, GLOBAL_END_INDEX);
        int sectionNumber = 1;
        for (LookUpTaskSection sect:sections) {
            log.debug("local section {}: [{}, {}]"
                    , sectionNumber++
                    , sect.getGlobalIndexLookUpStart()
                    , sect.getGlobalIndexLookUpEnd());
        }
    }
}
