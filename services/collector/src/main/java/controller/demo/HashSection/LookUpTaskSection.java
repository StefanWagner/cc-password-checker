package controller.demo.HashSection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;

public class LookUpTaskSection {
    private static final Logger log = LoggerFactory.getLogger(LookUpTaskSection.class);
    private static final int FILE_SIZE = 10_000_000;
    private final int globalIndexLookUpStart;
    private final int globalIndexLookUpEnd;

    public LookUpTaskSection(int globalIndexLookUpStart, int globalIndexLookUpEnd) {
        this.globalIndexLookUpStart = globalIndexLookUpStart;
        this.globalIndexLookUpEnd = globalIndexLookUpEnd;
    }

    public ArrayList<SubtaskSection> splitIntoSubtasks() {
        int fileNumberStartIndex = findContainingFileNumber(globalIndexLookUpStart);
        int fileNumberEndIndex = findContainingFileNumber(globalIndexLookUpEnd);

        int amountOfFiles = fileNumberEndIndex - fileNumberStartIndex;

        switch (amountOfFiles) {
            case 0:
                return createSingleFileSection(fileNumberStartIndex);
            case 1:
                return createTwoFileSection(fileNumberStartIndex, fileNumberEndIndex);
            default:
                return createMultipleFileSection(fileNumberStartIndex, fileNumberEndIndex);
        }
    }

    /**
     * file numbers start with 1
     *
     * @param globalIndex
     * @return
     */
    private int findContainingFileNumber(int globalIndex) {
        return ((globalIndex / FILE_SIZE) + 1);
    }

    private int findLocalIndex(int globalIndex) {
        return globalIndex % FILE_SIZE;
    }

    private ArrayList<SubtaskSection> createSingleFileSection(int fileNumber) {
        ArrayList<SubtaskSection> subtaskSections = new ArrayList<>();
        subtaskSections.add(
                new SubtaskSection(getFilenameFromNumber(fileNumber)
                        , findLocalIndex(globalIndexLookUpStart)
                        , findLocalIndex(globalIndexLookUpEnd))
        );
        return subtaskSections;
    }

    private ArrayList<SubtaskSection> createTwoFileSection(int firstFileNumber, int secondFileNumber) {
        ArrayList<SubtaskSection> subtaskSections = new ArrayList<>();
        //add first file
        subtaskSections.add(
                new SubtaskSection(getFilenameFromNumber(firstFileNumber)
                        , findLocalIndex(globalIndexLookUpStart)
                        , FILE_SIZE -1)
        );

        //add second file
        subtaskSections.add(
                new SubtaskSection(getFilenameFromNumber(secondFileNumber)
                        , 0
                        , findLocalIndex(globalIndexLookUpEnd))
        );

        return subtaskSections;
    }

    private ArrayList<SubtaskSection> createMultipleFileSection(int firstFileNumber, int lastFileNumber) {
        ArrayList<SubtaskSection> subtaskSections = createTwoFileSection(firstFileNumber, lastFileNumber);
        int filesBetweenFirstAndLast = lastFileNumber-firstFileNumber;

        for (int i = 1; i < filesBetweenFirstAndLast; i++) {
            subtaskSections.add(i, new SubtaskSection(getFilenameFromNumber(firstFileNumber+i)
                    , 0
                    , FILE_SIZE -1));
        }

        return subtaskSections;
    }

    /**
     * create the filename with the corr
     *
     * @param fileNumber
     * @return
     */
    private String getFilenameFromNumber(int fileNumber) {
        return "list/list" + fileNumber;
    }

    public int getGlobalIndexLookUpStart() {
        return globalIndexLookUpStart;
    }

    public int getGlobalIndexLookUpEnd() {
        return globalIndexLookUpEnd;
    }
}
