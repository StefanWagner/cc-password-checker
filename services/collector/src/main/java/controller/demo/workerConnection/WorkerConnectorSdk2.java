package controller.demo.workerConnection;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sns.adapter.Configuration;
import software.amazon.awssdk.auth.credentials.AwsSessionCredentials;
import software.amazon.awssdk.auth.credentials.InstanceProfileCredentialsProvider;
import software.amazon.awssdk.auth.credentials.StaticCredentialsProvider;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.sqs.SqsAsyncClient;
import software.amazon.awssdk.services.sqs.model.*;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;


public class WorkerConnectorSdk2 implements WorkerConnector {
    private static final Logger log = LoggerFactory.getLogger(WorkerConnectorSdk2.class);
    private static final String pushTaskUrl = "https://sqs.us-east-1.amazonaws.com/656465555881/PushTasks";
    private static final String progressUrl = "https://sqs.us-east-1.amazonaws.com/656465555881/Progress";
    private static final String interruptUrl = "https://sqs.us-east-1.amazonaws.com/656465555881/Interrupt";
    private static final ObjectMapper mapper = new ObjectMapper();
    private static final ConcurrentHashMap<String, List<LookUpTask>> taskMap = new ConcurrentHashMap<>();
    private static SqsAsyncClient sqs;
    List<Message> messageList = new ArrayList<>();

    public WorkerConnectorSdk2() {
        AwsSessionCredentials awsCreds = AwsSessionCredentials.create(Configuration.ACCESS_KEY_ID
                , Configuration.ACCESS_SECRET_KEY
                , Configuration.ACCESS_SESSION_TOKEN);

        sqs = SqsAsyncClient
                .builder()
                .region(Region.US_EAST_1)
                //.credentialsProvider(StaticCredentialsProvider.create(awsCreds))//TODO remove in production environment
                .credentialsProvider(InstanceProfileCredentialsProvider.create())
                .build();
    }

    @Override
    public String pushTasks(List<LookUpTask> lookUpTasks) {
        log.debug("pushing taksList to worker");
        //String uid = new UUID(16, 16).toString();
        String uid = UUID.randomUUID().toString();
        taskMap.put(uid, lookUpTasks);

        for (LookUpTask task : lookUpTasks) {
            sqs.sendMessage(SendMessageRequest.builder()
                    .queueUrl(pushTaskUrl)
                    .messageBody(task.toString())
                    .build());
        }
        return uid;
    }

    //External synchronisation
    @Override
    public synchronized int handleProgress(String tasksID) {
        if (!taskMap.containsKey(tasksID)) {
            log.error(tasksID + " does not exist (anymore?).");
            return -1;
        }
        log.debug("task map contains {} tasks", taskMap.get(tasksID).size());



        try {
            ReceiveMessageRequest receiveMessageRequest = ReceiveMessageRequest.builder()
                    .queueUrl(progressUrl)
                    .maxNumberOfMessages(10)
                    .build();
            messageList = sqs.receiveMessage(receiveMessageRequest).get().messages();

            if (messageList.isEmpty()) {
                return -1;
            }

            for (Message message : messageList) {
                ProgressMessage msg = null;

                msg = mapper.readValue(message.body(), ProgressMessage.class);

                for (LookUpTask task : this.taskMap.get(tasksID)) {
                    if (task.getId().equals(msg.getTaskID())) {
                        DeleteMessageRequest deleteMessageRequest = DeleteMessageRequest.builder()
                                .queueUrl(progressUrl)
                                .receiptHandle(message.receiptHandle())
                                .build();
                        //boolean deleted = sqs.deleteMessage(deleteMessageRequest).get().sdkHttpResponse().isSuccessful();
                        //log.info("Message deleted: {}", deleted );
                        sqs.deleteMessage(deleteMessageRequest);

                        if (msg.getIndex() == -1) {
                            log.info("Got -1 from worker.");

                            taskMap.get(tasksID).remove(task);

                            if (this.taskMap.get(tasksID).size() == 0) {
                                taskMap.remove(tasksID);
                                return -2;
                            } else {
                                return -1;
                            }
                        } else {
                            interruptTasks(tasksID);
                            log.info("Returning index: " + msg.getIndex());
                            taskMap.remove(tasksID);
                            return msg.getIndex();
                        }
                    }
                }
            }
        } catch (SqsException e) {
            log.error(e.awsErrorDetails().errorMessage());
        } catch (ExecutionException | InterruptedException e) {
            log.error(e.getMessage(), e);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            //e.printStackTrace();
            return -1;
        }
        return -1;
    }

    //OLD
      /*  ProgressMessage msg = null;
        List<Message> messageList;
        try {
            ReceiveMessageRequest receiveMessageRequest = ReceiveMessageRequest.builder()
                    .queueUrl(progressUrl)
                    .build();
            messageList = sqs.receiveMessage(receiveMessageRequest).get().messages();

            if (messageList.isEmpty()){
                return -1;
            }
            Message message = messageList.get(0);
            msg = mapper.readValue(message.body(), ProgressMessage.class);

            for (LookUpTask task : taskMap.get(tasksID)) {
                if (task.getId().equals(msg.getTaskID())) {

                    DeleteMessageRequest deleteMessageRequest = DeleteMessageRequest.builder()
                            .queueUrl(progressUrl)
                            .receiptHandle(message.receiptHandle())
                            .build();
                    boolean deleted = sqs.deleteMessage(deleteMessageRequest).get().sdkHttpResponse().isSuccessful();

                    log.info("Message deleted: {}", deleted );

                    if (msg.getIndex() == -1) {
                        log.info("Got -1 from worker.");
                        if (taskMap.get(tasksID).size() == 0) {
                            taskMap.remove(tasksID);
                            return -2;
                        } else {
                            taskMap.get(tasksID).remove(task);
                            if (taskMap.get(tasksID).isEmpty()) {
                                taskMap.remove(tasksID);
                                return -2;
                            }
                            return -1;
                        }
                    } else {
                        interruptTasks(tasksID);
                        log.info("Returning index: " + msg.getIndex());
                        taskMap.remove(tasksID);
                        return msg.getIndex();
                    }
                }
            }
        } catch (SqsException e) {
            log.error(e.awsErrorDetails().errorMessage());
        } catch (ExecutionException | InterruptedException e) {
            log.error(e.getMessage(), e);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            //e.printStackTrace();
            return -1;
        }

        return -1;*/


    //TODO Save completable futures in list and process after loop.
    private void interruptTasks(String tasksID) throws ExecutionException, InterruptedException {
        for (LookUpTask task : taskMap.get(tasksID)) {
            log.info("Trying to interrupt: " + tasksID);
            boolean success = sqs.sendMessage(SendMessageRequest.builder()
                    .queueUrl(interruptUrl)
                    .messageBody(task.getId())
                    .build()).get().sdkHttpResponse().isSuccessful();
        }
    }

    @Override
    public List<LookUpTask> getPendingTasks(String tasksID) {
        if (!taskMap.containsKey(tasksID)) {
            log.error(tasksID + " does not exist (anymore?).");
            return new ArrayList<>();
        } else {
            return taskMap.get(tasksID);
        }
    }

}
