package controller.demo.workerConnection;

import java.util.List;

public interface WorkerConnector {

    /**
     * Pushes a list of tasks workers via amazon queue.
     *
     * @param lookUpTasks List of tasks to be handled.
     * @return Returns a uuid (taskID) for identifying this list, needed in handleProgress
     */
    String pushTasks(List<LookUpTask> lookUpTasks);


    /**
     * This method handles the query of the list of tasks that were previously given to pushTasks by the user.
     *
     * @param tasksID Task ID received from pushTasks()....
     * @return Returns index if found, -1 if still pending, -2 if not found in tasks
     */
    int handleProgress(String tasksID);

    /**
     * Returns a list of tasks who did not return an index or that they did not find it. (Tasks who failed or are still working).
     *
     * @param tasksID Task ID received from pushTasks()
     * @return Returns a List of unfinished tasks.
     */
    List<LookUpTask> getPendingTasks(String tasksID);

}
