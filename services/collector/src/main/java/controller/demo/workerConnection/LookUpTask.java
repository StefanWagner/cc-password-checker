package controller.demo.workerConnection;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.ArrayList;

public class LookUpTask {

    private String id; //task id
    private String lookFor; // The actual SHA hashing that we are looking for, e.g.: AAAAAABCC

    private ArrayList<String> awsKeys; //file names, e.g.: /list/list1
    private ArrayList<Integer> startIndices; //startIndex [0] must be the same as last index of task before
    private ArrayList<Integer> endIndices;

    @Override
    public String toString() {
        try {
            return new ObjectMapper().writeValueAsString(this);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        return null;
    }

    public LookUpTask() {
        awsKeys = new ArrayList<>();
        endIndices = new ArrayList<>();
        startIndices = new ArrayList<>();
        id = "";
        lookFor = "";

    }

    public LookUpTask(ArrayList<String> awsKeys, String id, String lookFor, ArrayList startIndeces, ArrayList endIndeces) {
        this.awsKeys = awsKeys;
        this.id = id;
        this.lookFor = lookFor;
        this.startIndices = startIndeces;
        this.endIndices = endIndeces;
    }


    public ArrayList<String> getAwsKeys() {
        return awsKeys;
    }

    public void setAwsKeys(ArrayList awsKey) {
        this.awsKeys = awsKey;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLookFor() {
        return lookFor;
    }

    public void setLookFor(String lookFor) {
        this.lookFor = lookFor;
    }

    public ArrayList<Integer> getEndIndeces() {
        return endIndices;
    }

    public void setEndIndeces(ArrayList<Integer> endIndeces) {
        this.endIndices = endIndeces;
    }

    public ArrayList<Integer> getStartIndeces() {
        return startIndices;
    }

    public void setStartIndeces(ArrayList<Integer> startIndeces) {
        this.startIndices = startIndeces;
    }

    public boolean SectionEquals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        LookUpTask that = (LookUpTask) o;

        if (awsKeys != null ? !awsKeys.equals(that.awsKeys) : that.awsKeys != null) return false;
        if (startIndices != null ? !startIndices.equals(that.startIndices) : that.startIndices != null) return false;
        return endIndices != null ? endIndices.equals(that.endIndices) : that.endIndices == null;
    }

    @Override
    public int hashCode() {
        int result = awsKeys != null ? awsKeys.hashCode() : 0;
        result = 31 * result + (startIndices != null ? startIndices.hashCode() : 0);
        result = 31 * result + (endIndices != null ? endIndices.hashCode() : 0);
        return result;
    }
}