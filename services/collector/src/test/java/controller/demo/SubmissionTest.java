package controller.demo;

import controller.demo.HashSection.GlobalHashSection;
import controller.demo.frontendConnection.UserSubmission;
import controller.demo.workerConnection.LookUpTask;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class SubmissionTest {
    private static final Logger log = LoggerFactory.getLogger(SubmissionTest.class);
    private Submission submission;

    @BeforeEach
    void setUp() {
        submission = new Submission(new UserSubmission("asdf", "sdjfbdsjbfjsdhfbjhsdf"));
    }

    @Test
    void testIfTaskListIsCorrect() {
        int endIndex = submission.getGlobalHashSection().getGlobalEndIndex();

        for (int i = endIndex-10_000; i <endIndex; i++) {
            log.info("checking {} of {}", i, endIndex);
            ArrayList<LookUpTask> lookUpTasks = submission.createNextLookUpTasks();
            checkIfListNotEmpty(lookUpTasks);
            checkTasksUnique(lookUpTasks);
            submission.getGlobalHashSection().setGlobalStartIndex(i);
        }
    }



    private void checkIfListNotEmpty(ArrayList<LookUpTask> lookUpTasks){
        assertNotEquals(0, lookUpTasks.size());

    }

    private void checkTasksUnique(ArrayList<LookUpTask> lookUpTasks){
        for (LookUpTask task: lookUpTasks) {
            ArrayList<String> filename = task.getAwsKeys();
            ArrayList<Integer> startIndices = task.getStartIndeces();
            ArrayList<Integer> endIndices = task.getEndIndeces();

            for (int i = 0; i < task.getAwsKeys().size(); i++){
                for (int j = i+1; j < task.getAwsKeys().size(); j++){

                    assertFalse(checkSubtasksUnique(filename.get(i), startIndices.get(i), endIndices.get(i)
                            , filename.get(j), startIndices.get(j), endIndices.get(j)));
                }
            }


        }
    }

    private boolean checkSubtasksUnique(String filename1, int start1, int end1
            , String filename2, int start2, int end2){
        return filename1.equals(filename2) && start1 == start2 && end1 == end2;
    }
}