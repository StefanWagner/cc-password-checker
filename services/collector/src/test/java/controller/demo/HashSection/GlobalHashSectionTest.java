package controller.demo.HashSection;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;
@SpringBootTest
class GlobalHashSectionTest {
    private static GlobalHashSection hashSection;
    @BeforeAll
    static void beforeAll() {
        hashSection = new GlobalHashSection();
    }


    @Test
    void splitIntoSections_correctIndicesSet() {
        ArrayList<LookUpTaskSection> sections = hashSection.splitIntoLookUpTaskSections(10);
        assertEquals(10, sections.size());
        assertEquals(sections.get(2).getGlobalIndexLookUpEnd(), sections.get(3).getGlobalIndexLookUpStart());

        int lastSectionEndIndex = sections.get(sections.size()-1).getGlobalIndexLookUpEnd();
        assertEquals(hashSection.getGlobalEndIndex(), lastSectionEndIndex);
    }
}