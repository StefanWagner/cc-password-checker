package at.ac.univie.CC_A2_frontend_app;

import at.ac.univie.CC_A2_frontend_app.exceptions.BadRequestException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.test.util.ReflectionTestUtils;

import static org.junit.jupiter.api.Assertions.*;

/**
 * NOTE: tests throw a bunch of exceptions, because aws sns client doesn't have credentials
 * nevertheless testing without sns can be done
 */
class ApiControllerTest {
    @Autowired
    private ApiController apiController;
    private ClientController clientController;
    private MessageQueueController mqController;

    @BeforeEach
    void setUp() {
        apiController = new ApiController();
        clientController = new ClientController();
        MessageQueueController mqController = new MessageQueueController(clientController);

        ReflectionTestUtils.setField(apiController, "clientController", clientController);
        ReflectionTestUtils.setField(apiController, "messageQueueController", mqController);
    }

    @Test
    void submitNothing_shouldThrow() {
        assertThrows(BadRequestException.class, ()->apiController.submit(null));
        assertThrows(BadRequestException.class, ()->apiController.submit(""));
    }

    @Test
    void submitPassword_clientCreated() {
        String uuid = apiController.submit("password").getBody();
        assertNotNull(clientController.getClient(uuid));
    }
}