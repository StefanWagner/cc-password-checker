package at.ac.univie.CC_A2_frontend_app;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class HashManagementTest {
    @Test
    void hashString_compareToExternallyHashedString_shouldBeEquals() {
        String valueUsingHashManagement = HashManagement.computeSHA1("123e4567-e89b-42d3-a456-556642440000");

        Assertions.assertEquals("79C2C48484E47406FE83C45F3D749DD51B7622BD", valueUsingHashManagement);
    }
}
