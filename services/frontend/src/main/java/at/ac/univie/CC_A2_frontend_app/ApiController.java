package at.ac.univie.CC_A2_frontend_app;

import at.ac.univie.CC_A2_frontend_app.exceptions.BadRequestException;
import at.ac.univie.CC_A2_frontend_app.exceptions.NotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

@RestController
public class ApiController {
    private static final Logger log = LoggerFactory.getLogger(ApiController.class);
    @Autowired
    private ClientController clientController;
    @Autowired
    private MessageQueueController messageQueueController;

    @PostMapping("/submit")
    public ResponseEntity<String> submit(String password) {
        if (password == null ||password.isEmpty()) {
            throw new BadRequestException("password not given");
        }

        String clientUUID =  HashManagement.createUUID();
        clientController.addClient(clientUUID);
        messageQueueController.addSubmissionToQueue(clientUUID, HashManagement.computeSHA1(password));
        log.debug("client {} submitted to queue", clientUUID);

        return ResponseEntity.ok().body(clientUUID);
    }

    @GetMapping("/progress/{uuid}")
    public SseEmitter progress(@PathVariable String uuid){
        Client client = clientController.getClient(uuid);
        if (client == null) {
            throw new NotFoundException("Client "+ uuid + " not found!");
        }

        SseEmitter emitter = new SseEmitter();
        client.setClientConnection(emitter);
        log.debug("client emitter is set");
        return emitter;
    }
}
