package at.ac.univie.CC_A2_frontend_app;


import at.ac.univie.CC_A2_frontend_app.exceptions.BadRequestException;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import com.google.gson.stream.MalformedJsonException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import sns.adapter.Configuration;
import sns.adapter.SnsConnector;
import sns.adapter.Topics;

import java.io.IOException;


@Controller
public class MessageQueueController {
    private ClientController clientController;
    private SnsConnector snsConnector;
    private static final Logger log = LoggerFactory.getLogger(MessageQueueController.class);


    public MessageQueueController(@Autowired ClientController clientController) {
        this.clientController = clientController;
        snsConnector = new SnsConnector();
        //subscribe here or via aws website
        //aws website used
    }


    @PostMapping("/message")
    public ResponseEntity<?> receiveMessage(@RequestBody String body) {
        log.info("\n\n--------SNS Message Received----------\n"+body);
        String extractedMessage = extractJsonStringFromSnsNotification(body);
        if (!extractedMessage.isEmpty()){
            processJsonMessage(extractedMessage);
        }

        return ResponseEntity.ok().build();
    }

    /**
     * Appends a message with new user submission to AWS SNS queue
     *
     * @param passwordHash user input hashed width SHA1
     */
    public void addSubmissionToQueue(String uuid, String passwordHash) {
        String message = String.format("{'uuid':'%s', 'password': '%s'}", uuid, passwordHash);
        boolean success = snsConnector.sendMessage(message, Topics.submission);
        log.debug("submission sent, success: " + success);
    }

    private String extractJsonStringFromSnsNotification(String snsNotification) {
        try {
            JsonObject notificationMessage = JsonParser.parseString(snsNotification).getAsJsonObject();
            if (!notificationMessage.get("Type").getAsString().equals("Notification")){
                return "";
            }
            if (!notificationMessage.has("Message")) {
                throw new BadRequestException("no message provided");
            }
            return notificationMessage.get("Message").getAsString();
        } catch (JsonSyntaxException e) {
            throw new BadRequestException("not a valid json provided");
        }
    }

    private void processJsonMessage(String message) {
        try {
            JsonObject parsedMessage = JsonParser.parseString(message).getAsJsonObject();
            if (parsedMessage.has("uuid")
                    && parsedMessage.has("progress")) {
                if (parsedMessage.has("not_leaked")) {
                    //message has result
                    clientController.submitResult(parsedMessage.get("uuid").getAsString()
                            , parsedMessage.get("not_leaked").getAsBoolean());
                } else {
                    //message is just progress update
                    clientController.updateProgress(parsedMessage.get("uuid").getAsString()
                            , parsedMessage.get("progress").getAsInt());
                }
            }
            return;
        } catch (JsonSyntaxException e) {
            log.error("no valid json in sns message");
        }

        throw new BadRequestException("no valid message provided");
    }
}
