package at.ac.univie.CC_A2_frontend_app;

import org.apache.commons.codec.digest.DigestUtils;

import java.util.UUID;

public class HashManagement {

    public static String computeSHA1(String source) {
        return DigestUtils.sha1Hex(source).toUpperCase();
    }

    public static String createUUID() {
        return DigestUtils.sha256Hex(UUID.randomUUID().toString());
    }
}
