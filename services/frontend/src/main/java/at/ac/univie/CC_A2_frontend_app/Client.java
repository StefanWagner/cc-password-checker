package at.ac.univie.CC_A2_frontend_app;

import org.springframework.http.MediaType;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public class Client extends Thread {
    private static final Logger log = LoggerFactory.getLogger(Client.class);
    private final String uuid;
    private int progress;
    private int heartbeatIterations = 200;
    private SseEmitter clientConnection;

    public Client(String uuid) {
        this.uuid = uuid;
        this.progress = 0;
        this.clientConnection = null;
    }

    public synchronized void updateProgress(int progress) throws IOException {
        if (this.progress > progress){
            return;
        }

        this.progress = progress;
        if (clientConnection != null) {
            clientConnection.send(buildJsonString(uuid, progress), MediaType.APPLICATION_JSON);
        } else {
            log.info("Client {} has not connected yet", uuid);
        }
    }

    public synchronized boolean submitResult(boolean notLeaked) throws IOException {
        this.progress = 100;
        if (clientConnection == null) {
            throw new NullPointerException("Client not connected yet.");
        }
        clientConnection.send(buildJsonString(uuid, progress, notLeaked), MediaType.APPLICATION_JSON);
        clientConnection.complete();
        return true;
    }

    public synchronized void setClientConnection(SseEmitter emitter){
        this.clientConnection = emitter;
    }

    private String buildJsonString(String uuid, int progress) {
        StringBuilder builder = new StringBuilder("{");
        builder.append(String.format("\"uuid\":\"%s\",", uuid));
        builder.append("\"progress\":").append(progress).append("}");
        return builder.toString();
    }

    private String buildJsonString(String uuid, int progress, boolean not_leaked) {
        StringBuilder builder = new StringBuilder(buildJsonString(uuid, progress));
        builder.insert(builder.length()-1, ", \"not_leaked\":"+not_leaked);
        return builder.toString();
    }

    public String getUuid() {
        return uuid;
    }

    public int getProgress() {
        return progress;
    }

    public synchronized int getHeartbeatIterations() {
        return heartbeatIterations;
    }

    @Override
    public void run() {
        synchronized (this){
            while((progress < 100) && (heartbeatIterations > 0) ){
                try {
                    updateProgress(progress);
                    log.debug("sent heartbeat {}", heartbeatIterations--);
                    this.wait(3000);
                } catch (IOException e) {
                    log.error("error in heartbeat ",e);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
