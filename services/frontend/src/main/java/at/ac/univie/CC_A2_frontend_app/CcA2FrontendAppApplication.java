package at.ac.univie.CC_A2_frontend_app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CcA2FrontendAppApplication {
	public static void main(String[] args) {
		SpringApplication.run(CcA2FrontendAppApplication.class, args);
	}
}
