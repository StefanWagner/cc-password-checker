package at.ac.univie.CC_A2_frontend_app;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import java.io.IOException;
import java.util.concurrent.ConcurrentHashMap;

/**
 * holds data for progress of client submissions
 * Supports CRUD operation (like a database)
 */
@Component
public class ClientController {
    private static final Logger log = LoggerFactory.getLogger(ClientController.class);
    private static final ConcurrentHashMap<String, Client> clients = new ConcurrentHashMap<>();
    private static final int CONNECTION_RETRIES = 10;
    private static final int CONNECTION_RETRY_TIMEOUT = 1000;
    private static final Thread periodicLogger = logClients();


    public void addClient(String uuid) {
        clients.put(uuid, new Client(uuid));
        clients.get(uuid).start();
    }

    public Client getClient(String uuid) {
        return clients.get(uuid);
    }

    public void updateProgress(String uuid, int progress){
        try {
            if (clients.containsKey(uuid)) {
                clients.get(uuid).updateProgress(progress);
                log.debug("[update] [{}] {}%", uuid, progress);
            } else {
                log.info("Client uuid: {} not found", uuid);
            }
        } catch (IOException e) {
            log.warn("Could not connect to client {}", uuid);
        }
    }

    public void submitResult(String uuid, boolean not_leaked) {

        boolean success = false;
        int retries = CONNECTION_RETRIES;
        do {
            try {
                if (!clients.containsKey(uuid)){
                    return;
                }
                success = clients.get(uuid).submitResult(not_leaked);
                log.info("[update] [{}] not leaked = {}", uuid, not_leaked);
            } catch (IOException | NullPointerException e) {
                log.warn("Could not reach client {} : {}", uuid, e.getMessage());
                log.warn("Connecting retries left {}", retries);
            }

            try {
                if (!success) {
                    Thread.sleep(CONNECTION_RETRY_TIMEOUT);
                }
            } catch (InterruptedException e) {
                log.warn("Thread interrupted", e);
            }

            retries--;
        } while(!success && retries >0);


        clients.remove(uuid);
        log.debug("Client removed: {}", uuid);
    }


    private static Thread logClients(){
        Thread periodicLogger = new Thread(()->{
            while(true) {
                if (clients.isEmpty()){
                    try {
                        Thread.sleep(6000);
                    } catch (InterruptedException interruptedException) {
                        interruptedException.printStackTrace();
                    }
                    continue;
                }

                StringBuilder sb = new StringBuilder("\n\n\n");
                sb.append("-------------------Clients--------------------\n");
                for (String uuid:clients.keySet()) {
                    Client client = clients.get(uuid);
                    sb.append("| ")
                            .append(uuid.substring(14))
                            .append(" - progress: ")
                            .append(client.getProgress())
                            .append("% |\n");
                }

                sb.append("----------------------------------------------\n");
                log.info(sb.toString());

                cleanTimeoutClients();

                try {
                    Thread.sleep(6000);
                } catch (InterruptedException interruptedException) {
                    interruptedException.printStackTrace();
                }
            }
        });
        periodicLogger.start();
        return periodicLogger;
    }

    private static void cleanTimeoutClients(){
        for (Client client:clients.values()){
            if (client.getHeartbeatIterations()<=0){
                clients.remove(client.getUuid());
                log.info("client cleaned due to timeout: "+client.getUuid());
            }
        }
    }

}
