'use strict';
const serverBaseURL = "http://" + window.location.host;
const userInput = document.getElementById("user_input");
const progressFilled = document.getElementById("progress_filled");
const progressPercent = document.getElementById("progress_percent");
var length_ok = false;
var letters_ok = false;
var numeric_ok = false;
var special_chars_ok = false;
var uuid = "";


// listener to submit on keybord enter
userInput.addEventListener("keyup", function (event) {
    // Number 13 is the "Enter" key on the keyboard
    if (event.code === "Enter") {
        // Cancel the default action, if needed
        event.preventDefault();
        submitPassword();
    }
}); 


//is executed with onclick to the submit button 
function submitPassword() {
    let password = userInput.value;
    if ((password.length == 0) || (password.length > 32)) {
        return;
    }
    length_ok = passwordChecker.checkLength(password);
    letters_ok = passwordChecker.checkLetters(password);
    numeric_ok = passwordChecker.checkNumeric(password);
    special_chars_ok = passwordChecker.checkSpecialChars(password);
    submitPasswordToServer(password);
}

function submitPasswordToServer(password){
    console.log("submit password "+password+" to server");
    let xhr = new XMLHttpRequest();
    let data = new FormData();
    data.append("password", password);
    
    xhr.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            uuid = xhr.responseText;
            console.log("uuid = "+uuid);
            paneControllerModule.toggleProgressbar(true);
            listenProgressEvents();

        } else if (this.readyState == 4 && this.status >= 400){
            paneControllerModule.showErrorMsg(this.status);
        }
    };

    xhr.open("POST", serverBaseURL+"/submit", true);
    xhr.send(data);
}

function listenProgressEvents() {
    let evtSource = new EventSource(serverBaseURL+"/progress/"+uuid);
    console.log("progress request sent");
    evtSource.onmessage = function (event) {
        console.log("received data: "+event.data);
        let update_data = JSON.parse(event.data);
        if(update_data.uuid == uuid) {
            console.log("progress "+update_data.progress);
            paneControllerModule.updateProgressBar(update_data.progress);

            if (update_data.progress == 100 && update_data.not_leaked != undefined) {
                paneControllerModule.toggleProgressbar(false);
                paneControllerModule.showResultPane(length_ok, letters_ok, numeric_ok, special_chars_ok, update_data.not_leaked);
                evtSource.close();
            }
        }
    }
}
