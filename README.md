# CC Password Checker

## Description

With Password Checker one can type in a password and the application will check if this password already exists in a list of commonly used passwords. The common password list is a very large text file. Normally the list entries would be inserted in a database and a simple database requests would be performed. To learn about Amazone Web Services and cloud design patterns we implemented a search algorithm by ourself.

## Team

4 students

## Technology

Amazone Web Services (Simple Queue Service (SQS), Simple Notification Service (SNS), Elastic Beanstalk, Elastic Compute Cloud (EC2), Simple Storage Service (S3), Cloud Watch), Java 11, Spring Framework, JUnit 4, Logback, JavaScript, HTML, CSS

## Difficulties faced and lessons learned

Get in touch with Amazone Web Services and cloud design patterns.